# 3proxy-installer

- create_proxy_instance.sh : run it to create a t2.micro with 2 EIP, install and configure 3proxy, with 1 INCOMING and 1 OUTGOING IP. 
- check_proxy.sh CHECK|CHANGE_IP: run it whenever you want to check the avaiability of IP and change OUTGOING IP if proxy is not avaiable or just change OUTGOING IP . 


Note : 
- these script asume that you have installed the awscli and configure it properly. 
- create_proxy_instance.sh : asume that the default security group allow port SSH/22 and port sock5/8088
- create_proxy_instance.sh : need you to change the key pair name to appropriate key pair.
- check_proxy.sh : need you to configure the : INCOMING IP and the instance_id 1st
- Server which you use to run check_proxy.sh need has the private key of the key pair which was bind to the instance in AWS EC2.

