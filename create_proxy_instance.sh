#!/bin/bash

INSTALL_SCRIPT="3proxyinstall.sh"
IMAGE_ID=ami-090047a5066d1ac85
INSTANCE_TYPE="t2.micro"
KEY_PAIR=test01
# 
aws_create_instance () {
aws ec2 run-instances --instance-type ${INSTANCE_TYPE} --image-id ${IMAGE_ID} --key-name ${KEY_PAIR} > /tmp/new.instance.log
create_status=$?
INSTANCE_ID=`cat /tmp/new.instance.log | jq '.Instances | .[0]' | jq '.InstanceId' | xargs`
PRIMARY_PRIVATE_IP=`cat /tmp/new.instance.log | jq '.Instances | .[0]' | jq '.NetworkInterfaces' | jq '.[0]'  | jq '.PrivateIpAddresses' | jq '.[0]' | jq '.PrivateIpAddress' | xargs`
PRIMARY_INTERFACE_ID=`cat /tmp/new.instance.log | jq '.Instances | .[0]' | jq '.NetworkInterfaces' | jq '.[0]' | jq '.NetworkInterfaceId' | xargs`
if [[ (-z ${INSTANCE_ID}) || (-z ${PRIMARY_PRIVATE_IP}) || (-z ${PRIMARY_INTERFACE_ID}) ]]; then
        echo "Fail to create new instance"
        exit $create_status
else
        echo "Create new instance successfully, instance id : ${INSTANCE_ID}"
fi
}

aws_allocate_primaryIP () {
PRIMARY_IP=`aws ec2 describe-network-interfaces --network-interface-ids ${PRIMARY_INTERFACE_ID} | jq '.NetworkInterfaces | .[0]' | jq '.PrivateIpAddresses' | jq '. | .[0]' | jq '.Association' | jq '.PublicIp' | xargs`
if [ ${PRIMARY_IP} == "null" ]; then
	aws ec2 allocate-address --domain vpc > /tmp/primary_eip.tmp
	PRIMARY_IP=`cat /tmp/primary_eip.tmp | jq '.PublicIp' | xargs`
	PRIMARY_ALLOCATION=`cat /tmp/primary_eip.tmp | jq '.AllocationId' | xargs`
	echo "Getting new IP successfully, new IP: ${PRIMARY_IP}"
	aws ec2 associate-address --public-ip ${PRIMARY_IP} --instance-id ${INSTANCE_ID} --private-ip-address ${PRIMARY_PRIVATE_IP}
	associate_status=$?
	if [ ${associate_status} != "0" ]; then
		echo "Fail to associate IP ${PRIMARY_IP} to instance ${INSTANCE_ID}"
		return $associate_status
	else
		echo "Associate IP ${PRIMARY_IP} to instance ${INSTANCE_ID} successfully"
	fi
else
	echo "Instance already has primary EIP: $PRIMARY_IP"
fi
}

aws_allocate_secondaryIP () {
SECONDARY_IP=`aws ec2 describe-network-interfaces --network-interface-ids ${PRIMARY_INTERFACE_ID} | jq '.NetworkInterfaces | .[0]' | jq '.PrivateIpAddresses' | jq '. | .[1]' | jq '.Association' | jq '.PublicIp' | xargs`
if [ ${SECONDARY_IP} == "null" ]; then
	aws ec2 assign-private-ip-addresses --network-interface-id ${PRIMARY_INTERFACE_ID} --secondary-private-ip-address-count 1
	SECONDARY_PRIVATE_IP=`aws ec2 describe-network-interfaces --network-interface-ids ${PRIMARY_INTERFACE_ID} | jq '.NetworkInterfaces | .[0]' | jq '.PrivateIpAddresses' | jq '. | .[1]' | jq '.PrivateIpAddress' | xargs`
	echo "Getting new privateIP successfully, new IP: ${SECONDARY_PRIVATE_IP}"
	aws ec2 allocate-address --domain vpc > /tmp/secondary_eip.tmp
	SECONDARY_IP=`cat /tmp/secondary_eip.tmp | jq '.PublicIp' | xargs`
	echo "Getting new EIP successfully, new IP: ${SECONDARY_IP}"
	aws ec2 associate-address --public-ip ${SECONDARY_IP} --instance-id ${INSTANCE_ID} --private-ip-address ${SECONDARY_PRIVATE_IP}
	associate_2nd_status=$?
	if [ ${associate_2nd_status} != "0" ]; then
        	echo "Fail to associate 2nd EIP ${SECONDARY_IP} to instance ${INSTANCE_ID}"
        	return $associate_2nd_status
	else
        	echo "Associate IP ${SECONDARY_IP} to instance ${INSTANCE_ID} successfully"
	fi
else
	echo "Instance already has 2nd EIP: $SECONDARY_IP"
	SECONDARY_PRIVATE_IP=`aws ec2 describe-network-interfaces --network-interface-ids ${PRIMARY_INTERFACE_ID} | jq '.NetworkInterfaces | .[0]' | jq '.PrivateIpAddresses' | jq '. | .[1]' | jq '.PrivateIpAddress' | xargs`
fi
}

install_3proxy () {
scp ${INSTALL_SCRIPT} ubuntu@${PRIMARY_IP}:/tmp/
ssh -o StrictHostKeyChecking=no ubuntu@${PRIMARY_IP} "sudo bash /tmp/${INSTALL_SCRIPT}"
}

update_configuration () {
ssh -tt -o StrictHostKeyChecking=no ubuntu@${PRIMARY_IP} "sudo ip addr add $1 dev eth0"

ssh -tt -o StrictHostKeyChecking=no ubuntu@${PRIMARY_IP} "sudo sed -i -e 's/INCOMING/${1}/g' /etc/3proxy/3proxy.cfg && sudo sed -i -e 's/OUTGOING/${2}/g' /etc/3proxy/3proxy.cfg && sudo /etc/init.d/3proxy start"
}
# main 
aws_create_instance && echo "Pls wait 300s for the instance to be created and up"; sleep 300 && aws_allocate_primaryIP && aws_allocate_secondaryIP
# install 3proxy 
install_3proxy
update_configuration ${PRIMARY_PRIVATE_IP} ${SECONDARY_PRIVATE_IP}

