#!/bin/bash 

INCOMING_IP=A.B.C.D
PORT=8088
INSTANCE_ID=xxxxx


check_proxy () {
status=`curl -i -x socks5h://${INCOMING_IP}:${PORT} http://www.google.com/ 2> /dev/null | grep HTTP/1.1 | awk '{print $2}'`
}

aws_get_net_info () {
DEATH_OUTGOING_IP=`aws ec2 describe-addresses --filters "Name=instance-id,Values=${INSTANCE_ID}" | jq '.Addresses | .[1]' | jq '.PublicIp' | xargs`
DEATH_ALLOCATION=`aws ec2 describe-addresses --public-ip ${DEATH_OUTGOING_IP} | jq '.Addresses | .[0]' | jq '.AllocationId' | xargs`
OUTGOING_PRIVATE=`aws ec2 describe-addresses --public-ip ${DEATH_OUTGOING_IP} | jq '.Addresses | .[0]' | jq '.PrivateIpAddress' | xargs`

if [[ (-z ${DEATH_OUTGOING_IP}) || (-z ${DEATH_ALLOCATION}) || (-z ${OUTGOING_PRIVATE}) ]]; then
	echo "Error getting network information from AWS EC2"
	return 1
else
	echo "Getting network information from AWS EC2 successfully, death IP: ${DEATH_OUTGOING_IP}"
	return 0
fi
}

aws_allocate_newIP () {
aws ec2 allocate-address --domain vpc > /tmp/new_ip.tmp
NEW_OUTGOING_IP=`cat /tmp/new_ip.tmp | jq '.PublicIp' | xargs`
NEW_ALLOCATION=`cat /tmp/new_ip.tmp | jq '.AllocationId' | xargs`
echo "Getting new IP successfully, new IP: ${NEW_OUTGOING_IP}"
}

aws_associate_newIP () {
aws ec2 associate-address --public-ip ${NEW_OUTGOING_IP} --instance-id ${INSTANCE_ID} --private-ip-address ${OUTGOING_PRIVATE}
associate_status=$?
if [ $associate_status == "0" ]; then
	echo "Associate new IP to instance successfully. New outgoing IP: ${NEW_OUTGOING_IP}"
else
	echo "Fail to associate new IP to instance"
	return $associate_status
fi
}

aws_release_death_ip () {
aws ec2 release-address --allocation-id ${DEATH_ALLOCATION}
release_status=$?
if [ $release_status == "0" ]; then
        echo "Release old IP: ${DEATH_OUTGOING_IP} successfully"
else
	echo "Fail to release old IP: ${DEATH_OUTGOING_IP}"
        return $release_status
fi
}

# main
case $1 in 
	CHECK)
		check_proxy
		if [[ (-z ${status}) || (${status} != "200") ]]; then
			aws_get_net_info && aws_allocate_newIP && aws_associate_newIP && aws_release_death_ip
		fi
		;;
	CHANGE_IP)
		aws_get_net_info && aws_allocate_newIP && aws_associate_newIP && aws_release_death_ip
		;;
	*)
		echo "Pls run : check_proxy.sh CHECK|CHANGE_IP"
		;;
esac
