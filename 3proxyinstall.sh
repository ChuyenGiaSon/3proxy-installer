version=0.8.12
apt-get update && apt-get -y upgrade
apt-get install gcc make git -y
wget --no-check-certificate -O 3proxy-${version}.tar.gz https://codeload.github.com/z3APA3A/3proxy/tar.gz/${version}
tar xzf 3proxy-${version}.tar.gz
cd 3proxy-${version}
make -f Makefile.Linux
cd src
mkdir /etc/3proxy/
mv 3proxy /etc/3proxy/
cd /etc/3proxy/
wget -q --no-check-certificate https://gitlab.com/ChuyenGiaSon/3proxy-installer/raw/master/3proxy.cfg -O 3proxy.cfg
mkdir /var/log/3proxy/
cd /etc/init.d/
wget -q --no-check-certificate  https://gitlab.com/ChuyenGiaSon/3proxy-installer/raw/master/3proxy -O 3proxy
chmod  +x /etc/init.d/3proxy
update-rc.d 3proxy defaults
